const express = require('express')
const db = require('./db')
const utils = require('./utils')
const cors = require('cors')


const app = express()

app.use(cors('*'))
app.use(express.json())

app.get('/getAllMovies',(request,response)=>{
    const statement = `SELECT * FROM Movie`
    const connection = db.openConnection()
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error)
        response.send(utils.createResult(error))
        else
        response.send(utils.createResult(error,data))
    })
})

app.get('/getMovieByName/:title',(request,response)=>{
    const { title } = request.params
    const statement = `SELECT * FROM Movie WHERE movie_title = '${title}'`
    const connection = db.openConnection()
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error)
        response.send(utils.createResult(error))
        else
        response.send(utils.createResult(error,data))
    })
})

app.post('/addMovie',(request,response)=>{
    const { title , release_date , time , director_name } = request.body
    const statement = `
    INSERT INTO Movie
    VALUES
    (DEFAULT, '${title}', '${release_date}', '${time}', '${director_name}')`
    const connection = db.openConnection()
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error)
        response.send(utils.createResult(error))
        else
        response.send(utils.createResult(error,data))
    })
})

app.put('/updateMovie/:id',(request,response)=>{
    const { id } = request.params
    const { release_date , time } = request.body
    const statement = `
    UPDATE Movie 
    SET movie_release_date = '${release_date}' AND movie_time = '${time}'
    WHERE movie_id = ${id}
    `
    const connection = db.openConnection()
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error)
        response.send(utils.createResult(error))
        else
        response.send(utils.createResult(error,data))
    })
})

app.delete('/deleteMovie/:id',(request,response)=>{
    const { id } = request.params
    const statement = `DELETE FROM Movie WHERE movie_id = ${id}`
    const connection = db.openConnection()
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error)
        response.send(utils.createResult(error))
        else
        response.send(utils.createResult(error,data))
    })
})

app.listen(4000)