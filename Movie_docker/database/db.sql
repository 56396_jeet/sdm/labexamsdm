CREATE TABLE Movie (movie_id INT PRIMARY KEY AUTO_INCREMENT,
 movie_title VARCHAR(30), movie_release_date VARCHAR(20), movie_time VARCHAR(10), director_name VARCHAR(30));

 INSERT INTO Movie VALUES (DEFAULT, 'Saitama', '04 Apr 2020', '1hr 55mins', 'Jeet');
 INSERT INTO Movie VALUES (DEFAULT, 'Light', '06 June 2019', '2hr 30mins', 'Hrishi');
 INSERT INTO Movie VALUES (DEFAULT, 'Naruto', '07 July 2018', '2hr 10mins', 'Dd');